import React from "react";

import Table from "../../commons/tables/table";

const columns = [
  {
    Header: "Id",
    accessor: "id",
  },
  {
    Header: "User ID",
    accessor: "userId",
  },
  {
    Header: "Description",
    accessor: "description",
  },
  // Only for details
  // {
  // Header: "Address",
  // accessor: "address",
  // },
  {
    Header: "MHEC",
    accessor: "mhec",
  },
];

const filters = [
  {
    accessor: "name",
  },
];

function DeviceTable(props) {
  return (
    <Table
      data={props.tableData}
      columns={columns}
      search={filters}
      pageSize={5}
    />
  );
}

export default DeviceTable;
