import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
  monitoring: "/api/v1/monitoring",
};

function getDeviceData(params, callback) {
  let request = new Request(
    HOST.monitoring_api + endpoint.monitoring + "/" + params.id,
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth-token"),
      },
    }
  );
  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

export { getDeviceData };
