package ro.tuc.ds2020.controllers;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.mappers.UsersMapper;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.JwtService;
import ro.tuc.ds2020.services.MonitoringService;
import ro.tuc.ds2020.services.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/devices")
@CrossOrigin(origins = {"http://localhost", "http://localhost:3000"})
public class DeviceController {

    private final MonitoringService monitoringService;
    private final DeviceService deviceService;
    private final UserService userService;
    private final UsersMapper usersMapper;

    public DeviceController(MonitoringService monitoringService,
                            DeviceService deviceService,
                            UserService userService,
                            UsersMapper usersMapper) {
        this.monitoringService = monitoringService;
        this.deviceService = deviceService;
        this.userService = userService;
        this.usersMapper = usersMapper;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public ResponseEntity<Map<String, Object>> insertDevice(@Valid @RequestBody DeviceDetailsDTO deviceDetailsDTO) {
        MyLoggeer.print(deviceDetailsDTO.toString());
//        boolean response = usersMapper.checkUserById(deviceDetailsDTO.getUserId());
//        MyLoggeer.print("userId status: " + response);
        try{
            userService.findUserById(deviceDetailsDTO.getUserId());
        }catch(ResourceNotFoundException e){
            return new ResponseEntity<>(Map.of("user id", "invalid"), HttpStatus.NOT_FOUND);
        }
        UUID deviceId = deviceService.insert(deviceDetailsDTO);
        monitoringService.saveDevice(deviceId, deviceDetailsDTO);
        return new ResponseEntity<>(
                Map.of("id", deviceId), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getDevices() {
        MyLoggeer.print("getDevices called");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = (User) auth.getPrincipal();
        List<DeviceDTO> dtos;
        if (loggedUser.getRole() == Role.ROLE_ADMIN) {
            dtos = deviceService.findDevices();
        } else {
            dtos = deviceService.findDeviceByUserId(loggedUser.getId());
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDetailsDTO> getDeviceDetails(@PathVariable("id") UUID deviceId) {
        DeviceDetailsDTO deviceDetailsDTO = deviceService.findDeviceDetailsById(deviceId);
        return new ResponseEntity<>(deviceDetailsDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @PatchMapping(value = "/{id}")
    public ResponseEntity<DeviceDetailsDTO> updateDevice(@PathVariable("id") UUID deviceId,
            @Valid @RequestBody DeviceDetailsDTO deviceDetailsDTO) {
        MyLoggeer.print(deviceDetailsDTO.toString());
        boolean response = usersMapper.checkUserById(deviceDetailsDTO.getUserId());
        MyLoggeer.print("userId status: " + response);
        DeviceDetailsDTO dDetailsDTO = deviceService.update(deviceId, deviceDetailsDTO);
        monitoringService.updateDevice(dDetailsDTO.getId(), dDetailsDTO);
        return new ResponseEntity<>(dDetailsDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Map<String, String>> deleteUser(@PathVariable("id") UUID deviceId) {
        deviceService.deleteById(deviceId);
        monitoringService.deleteDevice(deviceId);
        return new ResponseEntity<>(
                Map.of("status", "success"), HttpStatus.OK);
    }

}
