package ro.tuc.ds2020.repositories;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
        Optional<User> findByEmail(String email);

        @Modifying
        @Transactional
        @Query(value = "update User u set " +
                "u.role = :role " +
                "where u.id = :id ")
        void updateUser(
                @Param(value = "id") UUID id,
                @Param(value = "role") Role role);
}
