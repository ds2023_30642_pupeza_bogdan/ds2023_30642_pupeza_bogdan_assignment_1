import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
  person: "/api/v1/users",
};

function getPersons(callback) {
  let request = new Request(HOST.users_api + endpoint.person, {
    method: "GET",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("auth-token"),
    },
  });
  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

function getPersonById(params, callback) {
  let request = new Request(HOST.users_api + endpoint.person + params.id, {
    method: "GET",
  });

  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

function postPerson(user, callback) {
  let request = new Request(HOST.users_api + endpoint.person, {
    method: "POST",
    headers: {
      accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("auth-token"),
      "Content-Type": "application/json",
    },
    body: JSON.stringify(user),
  });

  console.log("URL: " + request.url);

  RestApiClient.performRequest(request, callback);
}

function patchPerson(params, user, callback) {
  console.log("PARAMS: " + params.id);
  let request = new Request(
    HOST.users_api + endpoint.person + "/" + params.id,
    {
      method: "PATCH",
      headers: {
        accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("auth-token"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    }
  );

  console.log("URL: " + request.url);

  RestApiClient.performRequest(request, callback);
}

function deletePerson(params, callback) {
  let request = new Request(
    HOST.users_api + endpoint.person + "/" + params.id,
    {
      method: "DELETE",
      headers: {
        accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("auth-token"),
        "Content-Type": "application/json",
      },
    }
  );

  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

export { getPersons, getPersonById, postPerson, patchPerson, deletePerson };
