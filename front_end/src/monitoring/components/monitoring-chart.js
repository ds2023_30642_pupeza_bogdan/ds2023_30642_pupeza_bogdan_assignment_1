import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Chart } from "react-google-charts";
import { Card, Input, Label } from "reactstrap";
import * as API_MONITORING from "../api/monitoring-api";

const MonitoringChart = () => {
  const params = useParams();

  function formatDateTime(date) {
    return new Intl.DateTimeFormat("en-US", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }).format(date);
  }

  function formatDate(date) {
    return new Intl.DateTimeFormat("en-US", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
    }).format(date);
  }

  const inputInit = {
    value: formatDate(Date.now()),
    placeholder: "MM/DD/YYYY, HH:MM:SS AM/PM",
    valid: true,
    touched: false,
  };

  const [inputControl, setInputControl] = useState(inputInit);
  const [chartData, setChartData] = useState([]);
  const [filteredChartData, setFilteredChartData] = useState([]);
  const [error, setError] = useState({ status: 0, errorMessage: null });

  function handleChange(event) {
    let updatedDate = Date.parse(event.target.value);
    // fetchData();
    filterChartData(updatedDate);
  }

  function filterChartData(updatedDate) {
    console.log(updatedDate);
    let nextDate = updatedDate + 86400000; // one day

    if (!isNaN(updatedDate)) {
      let chartDataFiltered = chartData.filter((e) => {
        return (
          parseInt(e.timestamp) > updatedDate &&
          parseInt(e.timestamp) < nextDate
        );
      });
      console.log(formatDateTime(nextDate));
      console.log(chartDataFiltered);
      setFilteredChartData(chartDataFiltered);
    } else {
      console.log(chartData);
      setFilteredChartData(chartData);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  function fetchData() {
    const deviceId = params.deviceId;
    return API_MONITORING.getDeviceData(
      { id: deviceId },
      (result, status, err) => {
        if (result !== null && status === 200) {
          console.log(result);
          result.sort((a, b) => a.timestamp - b.timestamp);
          setChartData(result);
          filterChartData(Date.now());
          console.log(chartData);
        } else {
          setError((error) => ({ status: status, errorMessage: err }));
        }
      }
    );
  }

  function chartDataValues() {
    if (filteredChartData.length == 0) {
      return [["", 0]];
    }
    return filteredChartData.map((e) => [
      new Intl.DateTimeFormat("en-US", {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
      }).format(e.timestamp),
      parseInt(e.measurementValue),
    ]);
  }

  return (
    <div>
      <Card>
        <div
          className="form-group"
          style={{
            display: "flex",
            direction: "row",
            marginTop: "10px",
            justifyContent: "right",
            alignItems: "right",
            marginRight: "200px",
          }}
        >
          <Label style={{ marginTop: "5px" }} for="date">
            Filter by date:{" "}
          </Label>{" "}
          <Input
            style={{
              width: "205px",
              marginLeft: "10px",
            }}
            name="date"
            id="dateField"
            placeholder={inputControl.placeholder}
            onChange={handleChange}
            defaultValue={inputControl.value}
            required
          />
        </div>
      </Card>

      <Chart
        chartType="LineChart"
        data={[["Timestamp", "Consumption"], ...chartDataValues()]}
        options={{
          title: "Monitoring ",
        }}
        width={"100%"}
        height={"500px"}
      />
    </div>
  );
};

export default MonitoringChart;
