import React, { useState } from "react";
import Popup from "reactjs-popup";
import { Card, Container } from "reactstrap";
import SockJsClient from "react-stomp";

const WEBSOCKET_URL = "http://localhost:8082/api/v1/monitoring/websocket";

// WebSocket at server is configured with SockJs

const AppWebsocket = () => {
  if (localStorage.getItem("auth-token") == undefined) {
    return <div></div>;
  }
  const [messageList, setMessage] = useState([
    "Notifications will be displayed here.",
  ]);
  let onConnected = () => {
    console.log("Connected!");
    console.log(localStorage.getItem("user-id"));
  };

  let onMessageReceived = (msg) => {
    console.log("Message: " + msg.content);
    setMessage(messageList.concat("Device status: " + msg.content));
  };
  return (
    <Container>
      <Popup
        trigger={<div style={{ color: "white" }}>Notifications</div>}
        position="bottom left"
      >
        <Card>
          <div style={{ overflow: "scroll", height: "250px" }}>
            <ul>
              {messageList.map((message, index) => (
                <li key={index}>{message}</li>
              ))}
            </ul>
          </div>
        </Card>
      </Popup>
      <SockJsClient
        url={WEBSOCKET_URL}
        topics={[
          "/topic/monitoring",
          "/queue/monitoring/user/" + localStorage.getItem("user-id"),
        ]}
        onConnect={onConnected}
        onDisconnect={console.log("Disconnected!")}
        onMessage={(msg) => onMessageReceived(msg)}
        debug={false}
      />
    </Container>
  );
};

export default AppWebsocket;
