package ro.tuc.ds2020.mappers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.dtos.DevicesUserDTO;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.services.JwtService;

import java.util.Map;
import java.util.UUID;

public class UserMapper {

    private final String usersPath;
    private final RestTemplate restTemplate;
    private final JwtService jwtService;
    UserMapper(String usersPath, RestTemplate restTemplate, JwtService jwtService){
        this.usersPath = usersPath;
        this.restTemplate = restTemplate;
        this.jwtService = jwtService;
    }

    public HttpHeaders basicHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
    public HttpHeaders authHeaders(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String jwt = jwtService.generateToken(userDetails);
        var headers = basicHeaders();
        headers.setBearerAuth(jwt);
        return headers;
    }

    public boolean insertUser(DevicesUserDTO userDTO){
        try {
            HttpEntity<DevicesUserDTO> entity = new HttpEntity<>(userDTO, basicHeaders());
            var response = restTemplate.exchange(
                    usersPath,
                    HttpMethod.POST,
                    entity,
                    Map.class
            );
            MyLoggeer.print(response.toString());
        } catch (Exception e){
            //TODO: handle exception
            MyLoggeer.print(e.toString());
            return false;
        }
        return true;
    }
    public boolean deleteUserById(UUID userId){
        try {
            var response = restTemplate.exchange(
                    usersPath + "/" + userId,
                    HttpMethod.DELETE,
                    null,
                    Map.class
            );
            MyLoggeer.print(response.toString());
        } catch (Exception e){
            //TODO: handle exception
            MyLoggeer.print(e.toString());
            return false;
        }
        return true;
    }
}
