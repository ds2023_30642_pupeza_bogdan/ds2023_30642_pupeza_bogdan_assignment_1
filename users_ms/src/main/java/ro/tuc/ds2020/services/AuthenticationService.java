package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dao.request.SignUpRequest;
import ro.tuc.ds2020.dao.request.SigninRequest;
import ro.tuc.ds2020.dao.response.JwtAuthenticationResponse;

public interface AuthenticationService {
    JwtAuthenticationResponse signup(SignUpRequest request);

    JwtAuthenticationResponse signin(SigninRequest request);
}
