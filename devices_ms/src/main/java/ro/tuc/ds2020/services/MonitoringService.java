package ro.tuc.ds2020.services;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.config.RabbitMQConfig;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.my_logger.MyLoggeer;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

@Service
public class MonitoringService {

    private final RabbitTemplate rabbitTemplate;

    MonitoringService(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }
    public void saveDevice(UUID deviceId, DeviceDetailsDTO deviceDetailsDTO){
        Map<String, String> deviceMap = Map.of(
                "action", "SAVE",
                "id", deviceId.toString(),
                "user_id", deviceDetailsDTO.getUserId().toString(),
                "description", deviceDetailsDTO.getDescription(),
                "address", deviceDetailsDTO.getAddress(),
                "mhec", String.valueOf(deviceDetailsDTO.getMhec())
        );
        try {
            rabbitTemplate.convertAndSend(
                    RabbitMQConfig.QUEUE_NAME,
                    deviceMap.toString()
            );
        }catch (Exception e){
            MyLoggeer.print(e.getMessage());
        }
    }

    public void updateDevice(UUID deviceId, DeviceDetailsDTO deviceDetailsDTO){
        Map<String, String> deviceMap = Map.of(
                "action", "UPDATE",
                "id", deviceId.toString(),
                "user_id", deviceDetailsDTO.getUserId().toString(),
                "description", deviceDetailsDTO.getDescription(),
                "address", deviceDetailsDTO.getAddress(),
                "mhec", String.valueOf(deviceDetailsDTO.getMhec())
        );
        try {
            rabbitTemplate.convertAndSend(
                    RabbitMQConfig.QUEUE_NAME,
                    deviceMap.toString()
            );
        }catch (Exception e){
            MyLoggeer.print(e.getMessage());
        }
    }

    public void deleteDevice(UUID deviceId){
        Map<String, String> deviceMap = Map.of(
                "action", "DELETE",
                "id", deviceId.toString()
        );
        try {
            rabbitTemplate.convertAndSend(
                    RabbitMQConfig.QUEUE_NAME,
                    deviceMap.toString()
            );
        }catch (Exception e){
            MyLoggeer.print(e.getMessage());
        }
    }
}
