import React, { useState, useEffect } from "react";
import { Col, Row } from "reactstrap";
import { FormGroup, Input, Label } from "reactstrap";
import Button from "react-bootstrap/Button";

import * as API_DEVICES from "../api/device-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import Validate from "./validators/device-validators";

const DeviceForm = (props) => {
  const formControlsInit = {
    description: {
      value: props.device.description,
      placeholder: "Description...",
      valid: props.edit,
      touched: false,
      validationRules: {
        minLength: 3,
        isRequired: true,
      },
    },
    mhec: {
      value: props.device.mhec,
      placeholder: "MHEC...",
      valid: props.edit,
      touched: false,
    },
    address: {
      value: props.device.address,
      placeholder: "Address...",
      valid: props.edit,
      touched: false,
    },
    userId: {
      value: props.device.userId,
      placeholder: "User ID...",
      valid: props.edit,
      touched: false,
    },
    //TODO: add user id for add by ADMIN
  };

  const [error, setError] = useState({ status: 0, errorMessage: null });
  const [formIsValid, setFormIsValid] = useState(false);
  const [formControls, setFormControls] = useState(formControlsInit);

  function handleChange(event) {
    let name = event.target.name;
    let value = event.target.value;

    let updatedControls = { ...formControls };

    let updatedFormElement = updatedControls[name];

    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = Validate(
      value,
      updatedFormElement.validationRules
    );
    updatedControls[name] = updatedFormElement;

    let formIsValid = true;
    for (let updatedFormElementName in updatedControls) {
      formIsValid =
        updatedControls[updatedFormElementName].valid && formIsValid;
    }

    setFormControls((formControls) => updatedControls);
    setFormIsValid((formIsValidPrev) => formIsValid);
  }

  function registerDevice(device) {
    return API_DEVICES.postDevice(device, (result, status, err) => {
      if (result !== null && (status === 200 || status === 201)) {
        console.log("Successfully inserted device with id: " + result);
        props.reloadHandler();
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  function updateDevice(params, device) {
    console.log("updateDevice: " + params);
    return API_DEVICES.patchDevice(params, device, (result, status, err) => {
      if (result !== null && (status === 200 || status === 201)) {
        console.log("Successfully update device with id: " + result);
        props.reloadHandler();
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  function handleSubmit() {
    let device = {
      description: formControls.description.value,
      mhec: formControls.mhec.value,
      address: formControls.address.value,
      userId: formControls.userId.value,
    };
    if (props.edit === true) {
      let params = {
        id: props.device.id,
      };
      updateDevice(params, device);
    } else {
      registerDevice(device);
    }
  }

  return (
    <div>
      <FormGroup id="description">
        <Label for="descriptionField"> Description: </Label>
        <Input
          name="description"
          id="descriptionField"
          placeholder={formControls.description.placeholder}
          onChange={handleChange}
          defaultValue={formControls.description.value}
          touched={formControls.description.touched ? 1 : 0}
          valid={formControls.description.valid}
          required
        />
        {formControls.description.touched &&
          !formControls.description.valid && (
            <div className={"error-message row"}>
              {" "}
              * Description must have at least 3 characters{" "}
            </div>
          )}
      </FormGroup>

      <FormGroup id="userId">
        <Label for="userIdField"> User ID: </Label>
        <Input
          name="userId"
          id="userIdField"
          placeholder={formControls.userId.placeholder}
          onChange={handleChange}
          defaultValue={formControls.userId.value}
          touched={formControls.userId.touched ? 1 : 0}
          valid={formControls.userId.valid}
          required
        />
      </FormGroup>

      <FormGroup id="address">
        <Label for="addressField"> Address: </Label>
        <Input
          name="address"
          id="addressField"
          placeholder={formControls.address.placeholder}
          onChange={handleChange}
          defaultValue={formControls.address.value}
          touched={formControls.address.touched ? 1 : 0}
          valid={formControls.address.valid}
          required
        />
      </FormGroup>

      <FormGroup id="mhec">
        <Label for="mhecField"> MHEC: </Label>
        <Input
          name="mhec"
          id="mhecField"
          placeholder={formControls.mhec.placeholder}
          min={0}
          max={100}
          type="number"
          onChange={handleChange}
          defaultValue={formControls.mhec.value}
          touched={formControls.mhec.touched ? 1 : 0}
          valid={formControls.mhec.valid}
          required
        />
      </FormGroup>

      <Row>
        <Col sm={{ size: "4", offset: 8 }}>
          <Button
            type={"submit"}
            disabled={!formIsValid}
            onClick={handleSubmit}
          >
            {" "}
            Submit{" "}
          </Button>
        </Col>
      </Row>

      {error.status > 0 && (
        <APIResponseErrorMessage
          errorStatus={error.status}
          error={error.errorMessage}
        />
      )}
    </div>
  );
};

export default DeviceForm;
