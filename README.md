# DS Project

## Setup +  Requirements
* mvn 3.9.5
* java 17.0.6
* node 16.20.2
* npm 8.19.4
* mysql
* Intellij + VSCode
* Docker
* back-end init: add configuration in IntelliJ at ```ro.tuc.ds2020.Ds2020Application```
* back-end start: click on run button (Shift + F10) 
* front-end init: ```npm install```
* front-end start: ```npm start```

## DOCKER set up
### MySql Server
* ```docker run -it --name mysqldb-users --network springboot-mysql-net -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=users-db -e MYSQL_USER=sys -e MYSQL_PASSWORD=1234 -d mysql:5.7```
* ```docker run -it --name mysqldb-devices --network springboot-mysql-net -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=devices-db -e MYSQL_USER=sys -e MYSQL_PASSWORD=1234 -d mysql:5.7```

### Users Microservice
* ```docker build -t users_ms_image .```
* ```docker run --network springboot-mysql-net --name users_ms -p 8080:8080 users_ms_image_2```

### Devices Microservice
* ```docker build -t devices_ms_image .```
* ```docker run --network springboot-mysql-net --name devices_ms -p 8081:8081 devices_ms_image```


### Front End
* ```docker build -t  fe-image  . ```
* ```docker-compose up -d ```


## MySQL commands
* ```mysql -u sys/root -p ```
* password: ```1234```
* ```use users-db```
* ```select * from user```
* ```update user u set u.role="ROLE_ADMIN" where u.email="bp@admin.com"```

* ```use devices-db```
* ```select * from user```
* ```update user u set u.role="ROLE_ADMIN" where u.email="bp@admin.com"```

## Endpoints
### Auth
- POST localhost:8080/api/v1/auth/signup + body
- POST localhost:8080/api/v1/auth/signin + body
### Users 
- GET localhost:8080/api/v1/users // all users
- GET localhost:8080/api/v1/users/{{user_id}} // user with id user_id
- POST localhost:8080/api/v1/users + body // creates user with data from body
- PATCH localhost:8080/api/v1/users/{{user_id}} + body: JSON // updates user with id user_id 
- DELETE localhost:8080/api/v1/users/{{user_id}} // deletes user with id user_id 
- GET localhost:8080/api/v1/users/{{user_id}}/devices // devices of the user with id user_id
### Devices
- GET localhost:8081/api/v1/devices // all users
- GET localhost:8081/api/v1/devices/{{device_id}} // device with id device_id
- POST localhost:8081/api/v1/devices + body // creates device with data from body
- PATCH localhost:8081/api/v1/devices/{{device_id}} + body: JSON // updates device with id device_id
- DELETE localhost:8081/api/v1/devices/{{device_id}} // deletes device with id device_id

## Conceptual Architecture

![](conceptual_architecture.png)

## Deployment Diagram

![](deployment_diagram.png)

## CORS config for Spring Security

- Add `@CrossOrigin(origins = {"http://localhost", "http://localhost:3000"})` at the beginning of the `@Controller` class
- Add the `@Controller`'s mapping path to the filter chain in `SecurityConfiguration`
